$(document).ready(function($) { 

	// owl control
	$('.next').on('click',function(e){
		$(this).closest('section').find('.owl-next').trigger(e);
	});

	$('.prev').on('click',function(e){
		$(this).closest('section').find('.owl-prev').trigger(e);
	});

	$('.home_reviews__carousel').owlCarousel({
		items: 1,
		dots: false,
		loop: true
	});

	$('.field_wrap textarea, .field_wrap input').on('focusout', function(){
		if($(this).val()=='') {
			$(this).parent().removeClass('filled');
		} else {
			$(this).parent().addClass('filled');
		}
	});

	$('.field_wrap.select input').on('click', function(){
		$(this).parent().toggleClass('open').find('.options').slideToggle();
	});

	$('.field_wrap.select input').on('focusout', function(){
		$(this).parent().removeClass('open').find('.options').slideUp();
	});

	$('.field_wrap.select .options div').on('click', function(){
		$(this).closest('.field_wrap').find('input').val($(this).text()).trigger('focusout');
	});

	$('.header_menu .burger').on('click', function(){
		$(this).toggleClass('open');
		$('.header_menu ul').slideToggle();
	});

});